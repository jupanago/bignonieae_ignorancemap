# Testing raster reclassification under three different codification schemes

# This script shows how a codification scheme was applied to the raster layers
# of the Sampling Bias analysis, the Survey Effort analysis, and the cells 
# without records inside the geographical extend of Bignonieae to produce a map 
# of ignorance. This is a categorical map that synthesizes the analyses into a 
# unique figure. It represents a summary of these analyses in a cell per cell
# basis: it says for a determined cell what was the result obtained in each 
# analysis. 
#
# To produce this map, we reclassified each layer with a numeric code. The 
# rationale to choose this code was to discriminate clearly among the possible
# cell types that can be produced once you combined the information from the
# analyses. The information to combined was:
# 
# (1) the empty cells (No occurrence data - dubbed "NO"), 
# (2) the cells with the highest bias and lowest sampling rates 
# (extracted from the results of sampbias::calcvulate_bias() - dubbed "SB"), and 
# (3) the cells with the poorest survey effort (extracted from the results of 
# KnowBR::SurveyQ() - dubbed "PS").
#
# After adding these layers seven different types of cells can be obtained:
#
# Cells with only one kind of information:
# 
# i. NO
# ii. PS
# iii. SB 
#
# Cells with mixed information:
#
# iv. NO + PS (*)
# v. NO + SB 
# vi. PS + SB 
# vii. NO + PS + SB (*)
#
# (*) NOTE: The cell types (iv) and (vii) cannot be obtained in the analysis
# because the no occurrence (NO) and the poor surveyed (PS) are are incompatible.
# KnowBR calculates the survey effort only in cells that have occurrence records.
# Therefore, no cell will be obtained with the condition of "NO + PS" or "NO + PS + SB".
# 
#
# The chosen numerical codification scheme allowed to discriminate these seven
# types of cells unambiguously. It was chosen for a matter of convenience to
# represent in a map the information from all the analyses. Below we show that
# although the codification scheme can be arbitrary, they fulfill the objective
# of portraying the results at once in a map. This condition will be maintained
# in any case in which the sum of all possible combinations among an 
# increasing arithmetic sequence of three numbers results in seven different
# numbers. 
#
# The chosen codification scheme: 
# i. No occurrences raster (NO): 1
# ii. Poor Surveyed raster (PS): 3
# iii. Bias factor raster (SB): 5
#
# Adding these raster layers will produce a final raster layer with the values:
# 1, 3, 4, 5, 6, 8, and 9. These values identified unambiguously all the possible
# combinations of information per cell. The table below shows the codification 
# schemes used to support our choice. Note that the alternative codification 
# schemes do not discriminate clearly between cell types that involved the sum 
# of "NO" and "PS" (lines 6 and 7). 
#
# TABLE 1. Codification schemes
#
#    |                  | Original       | Alter_246      | Alter_123      |
#    | Cell type        | NO:1 PS:3 SB:5 | NO:2 PS:4 SB:6 | NO:1 PS:2 SB:3 |
#    |:----------------:|:--------------:|:--------------:|:--------------:|
# 1  |ǸO                |1	             |2               |1               |
# 2  |PS                |3               |4               |2               |
# 3  |NO + PS (*)       |4               |**6**           |**3**           |
# 4  |SB                |5               |**6**           |**3**           |
# 5  |NO + SB           |6               |8               |4               |
# 6  |PS + SB           |8               |10              |5               |
# 7  |NO + PS +SB       |9               |12              |6               |

# The numerical code is used descriptively and has no consequences in the 
# interpretation of the results. We wanted to have a map pointing to the regions
# that we ignore the most given the information provided by the database and 
# considering the biases related to accessibility issues. The map obtained gives
# us that information.
#
# See below in the maps produced using these different codification schemes
# that our resulting maps are the same. Using higher values can change the color
# scale but the interpretation of the cell types continues the same. 

# -------------------

# 1. Libraries ####

# Install libraries
install.packages(c("tidyverse", "raster", "rgdal", "rnaturalearth"))

library(tidyverse)
library(raster)
library(rnaturalearth)
library(spatial.tools)

# NOTE: The package spatial.tools isn't available for the most recent R version.
# I reproduce here the function spatial.tools::modify_raster_margins() that was
# the function I used from this package. 

modify_raster_margins <- function(x,extent_delta=c(0,0,0,0),value=NA) {
  
  x_extents <- extent(x)
  
  res_x <- res(x)
  
  
  
  x_modified <- x
  
  
  
  if(any(extent_delta < 0))
    
  {
    
    # Need to crop
    
    # ul:
    
    ul_mod <- extent_delta[c(1,3)] * res_x
    
    ul_mod[ul_mod > 0] <- 0
    
    lr_mod <- extent_delta[c(2,4)] * res_x
    
    lr_mod[lr_mod > 0] <- 0
    
    # This works fine, but for some reason CRAN doesn't like it:	
    
    #	crop_extent <- as.vector(x_extents)
    
    crop_extent <- c(x_extents@xmin,x_extents@xmax,x_extents@ymin,x_extents@ymax)
    
    crop_extent[c(1,3)] <- crop_extent[c(1,3)] - ul_mod
    
    crop_extent[c(2,4)] <- crop_extent[c(2,4)] + lr_mod
    
    
    
    x_modified <- crop(x_modified,crop_extent)
    
  }
  
  
  
  if(any(extent_delta > 0))
    
  {
    
    # Need to crop
    
    # ul:
    
    ul_mod <- extent_delta[c(1,3)] * res_x
    
    ul_mod[ul_mod < 0] <- 0
    
    lr_mod <- extent_delta[c(2,4)] * res_x
    
    lr_mod[lr_mod < 0] <- 0
    
    #		Again, a hack for CRAN?		
    
    #		extend_extent <- as.vector(x_extents)
    
    extend_extent <- c(x_extents@xmin,x_extents@xmax,x_extents@ymin,x_extents@ymax)
    
    extend_extent[c(1,3)] <- extend_extent[c(1,3)] - ul_mod
    
    extend_extent[c(2,4)] <- extend_extent[c(2,4)] + lr_mod
    
    
    
    x_modified <- extend(x_modified,extend_extent,value=value)
    
  }
  
  
  
  return(x_modified)
  
}

# 2. Data ####

# The data below come from:
# i. Results from sampling bias analysis using sampbias::calculate_bias(). The
# sampling bias factor geographical projection was taken from the sampbias object.
# ii. Survey quality analysis in KnowBR using the function KnownBR::SurveyQ().
# Data from poor surveyed cells was extracted. 
# iii. Database point occurrences in cells of 1 degree. Empty cells were taken 
# from sambias object.

# These three sources of data are represented as rasters and reclassified below.

# i. Bias factor from sampbias analysis
sampbias_ras <- raster("./output/Sampbias/Sampbias_raster_ALLBFACTORS.tif")
# ii. SurveyQ matrix from KnownBR analysis
SurveyQ_ras <- readRDS(file = "./output/KnowBR_output/v2/SurveyQ_matrix.rds")
# iii. Occurrences taken for convenience from sampbias object
sbias_object <- readRDS("./output/Sampbias/SampbiasV2_object.rds")

# 3. Data processing 

# Raster layers were processed to have the same number of cells, origin, and extent.
# It was necessary to increase one row and one column in rasters from sampbias.

# i. Bias factors
sampbias_ras <- modify_raster_margins(sampbias_ras, extent_delta = c(1,0,1,0),
                                      value = NA) 
# ii. Survey analysis
SurveyQ_ras <- raster(SurveyQ_ras)
extent(SurveyQ_ras) <- extent(sampbias_ras)

# iii. Ocurrences
occ_ras <- sbias_object$occurrences
occ_ras <- modify_raster_margins(occ_ras, extent_delta = c(1,0,1,0), value = NA) 
occ_ras <- mask(occ_ras, sampbias_ras)

# 4. Raster reclassification ####
# 4.1. Original codification scheme: (NO:1 PS:3 SB:5) ####

# Reclassifying sambias_ras: 
# We reclassified the raster to separate the regions with lowest sampling rates. 
# The lowest sampling rates were chosen as the sampling rates below the average
# of the range of sampling rate values and non-inclusive. 
# max(values(sampbias_ras), na.rm = T) # max:1.395441
# min(values(sampbias_ras), na.rm = T) # min: 0.2109771
# (1.395441 + 0.2109771) / 2 = 0.803209. 
# Threshold for lowest sampling rates: 0.7

sampbias_rec <- calc(sampbias_ras, fun = function(x) {x[x > 0.2 & x <= 0.7] <- 10; return(x)})
sampbias_rec <- calc(sampbias_rec, fun = function(x) {x[x < 10] <- 0;  return(x)})
sampbias_rec <- calc(sampbias_rec, fun = function(x) {x[x == 10] <- 5; return(x)})

# The values of the raster layer produced by KnowBR::SurveyQ() were: -9999, 1, 2, 3. 
# 1 stands for "High", 2 for "Fair", and 3 for "Poor". -9999 the rest of cells.
# We replaced all the values except 3 with 0, to extract only the cells with 
# poor surveys.

SurveyQ_ras[SurveyQ_ras %in% c(1,2,-9999)] <- 0

# The occurrence records raster layer has the number of species records per cell. 
# To extract the empty cells, we assigned the value of 1 to the empty cells, and 
# the value of 0 to the cells with at least 1 record.  

occ_ras <- calc(occ_ras, fun=function(x){ x[x > 0] <- 10; return(x)})
occ_ras <- calc(occ_ras, fun=function(x){ x[x < 10] <- 1; return(x)})
occ_ras <- calc(occ_ras, fun=function(x){ x[x == 10] <- 0; return(x)})

# Plot of reclassified layers

par(mfrow = c(2,2))
plot(sampbias_rec)
plot(SurveyQ_ras)
plot(occ_ras)

# Final raster

# The nonexistence of the "NO + PS" cell type is demonstrated by the sum of the 
# reclassified layers occ_ras (NO) and SurveyQ_ras (PS). Following the chosen
# codification scheme (NO:1 and PS:3), we don't have cells with the value of 4.

ignorance_ras <- occ_ras + SurveyQ_ras 
unique(values(ignorance_ras)) # [1] NA  1  0  3

# After adding the sampling bias layer (SP), the values obtained must be
# concordant with the chosen codification: 1 3 5 6 8. (Note that 4 will be
# absent).

Surv_igno_ras <- ignorance_ras + sampbias_rec
unique(values(Surv_igno_ras)) # [1] NA  1  0  3  5  6  8

# Plot of reclassified layers and the final raster layer: ignorance. 
par(mfrow = c(2, 2), mar = c(4, 4, 4, 4))
plot(occ_ras)
title(main = "No Occurrence (NO)", 
      sub = "Reclassified value 1\n 0: land, NA: sea",
      font.sub = 3, col.sub = "blue", cex.sub = 1.5)
plot(SurveyQ_ras)
title(main = "SurveyQ - Poor (PS)", 
      sub = "Reclassified value 3\n 0: everything else",
      font.sub = 3, col.sub = "blue", cex.sub = 1.5)
plot(sampbias_rec)
title(main = "sampbias - lowest rates (SB)", 
      sub = "Reclassified value 5\n 0: land, NA: sea",
      font.sub = 3, col.sub = "blue", cex.sub = 1.5)
plot(Surv_igno_ras)
title(main = "Final raster (Ignorance)", 
      sub = "Cell types: 1, 3, 5, 6, 8\n 0: land, NA: sea",
      font.sub = 3, col.sub = "blue", cex.sub = 1.5)

# 4.2. Testing the alternative coding schemes: Maps ####

# The following function produces a figure with 4 maps: 1) No Occurrence (NO) 
# layer, 2) SurveyQ - Poor (PS) layer, 3) sampbias - lowest rates layer (SB), 
# and 4) Final raster after adding the previous three layers (Ignorance). This
# figure will help to see the maps under different codification schemes. 

# The function also creates the raster layers under each codification scheme and 
# a data frame describing the scheme used. All of them are available for
# inspection and use. These objects will be used below for another test. 

# Function checking_codification()
# Arguments:
# 1) rec_SB: numeric value to reclassify the layer from sampbias analysis.
# 2) rec_PS: numeric value to reclassify the layer from KnownBR::SurveyQ().
# 3) rec_NO: numeric value to assign to empty cells in the occurrence raster layer.
# 4) coding_alt: Default value = "code_id". A name provided by the user to 
# identify the coding scheme. 
#
# Outputs:
# 1) Maps of each raster layer
# 2) Raster layers under the specified codification scheme.
# 3) A data frame of the cell types and their numerical codification.

checking_codification <- function(rec_SB, rec_PS, rec_NO, 
                                  coding_alt = "code_id") {
  
  # Data
  # i. Bias factor from sampbias analysis
  sampbias_ras <- raster("./output/Sampbias/Sampbias_raster_ALLBFACTORS.tif")
  # ii. SurveyQ matrix from KnownBR analysis
  SurveyQ_ras <- readRDS(file = "./output/KnowBR_output/v2/SurveyQ_matrix.rds")
  # iii. Occurrences taken for convenience from sampbias object
  sbias_object <- readRDS("./output/Sampbias/SampbiasV2_object.rds")
  
  # Raster processing
  # i. Bias factors
  sampbias_ras <- modify_raster_margins(sampbias_ras, extent_delta = c(1,0,1,0),
                                        value = NA) 
  # ii. Survey analysis
  SurveyQ_ras <- raster(SurveyQ_ras)
  extent(SurveyQ_ras) <- extent(sampbias_ras)
  # iii. Ocurrences
  occ_ras <- sbias_object$occurrences
  occ_ras <- modify_raster_margins(occ_ras, extent_delta = c(1,0,1,0), value = NA) 
  occ_ras <- mask(occ_ras, sampbias_ras)
  
  # Reclassification
  
  sampbias_rec <- calc(sampbias_ras, fun = function(x) {x[x > 0.2 & x <= 0.7] <- 10; return(x)})
  sampbias_rec <- calc(sampbias_rec, fun = function(x) {x[x < 10] <- 0;  return(x)})
  sampbias_rec <- calc(sampbias_rec, fun = function(x) {x[x == 10] <- rec_SB; return(x)})
  
  # The values of the raster produced by KnowBR::SurveyQ() are: -9999, 1, 2, 3. 
  # 1 stands for Good, 2 for Fair, and 3 for Poor. -9999 identified the unconsidered
  # cells.
  # We replaced all the values except 3 with 0, to extract only the cells with 
  #poor surveys.
  
  SurveyQ_ras[SurveyQ_ras %in% c(1,2,-9999)] <- 0
  
  if (rec_PS != 3) {
    SurveyQ_ras[SurveyQ_ras %in% c(3)] <- rec_PS
  }
  
  # The occurrence raster records the number of species records per cell. 
  # To extract the empty cells we assigned the value of 1 to the empty cells, and 
  # the value of 0 to the cells with at least 1 record.  
  
  occ_ras <- calc(occ_ras, fun=function(x){ x[x > 0] <- 10; return(x)})
  occ_ras <- calc(occ_ras, fun=function(x){ x[x < 10] <- rec_NO; return(x)})
  occ_ras <- calc(occ_ras, fun=function(x){ x[x == 10] <- 0; return(x)})
  
  # Final raster
  ignorance_ras <- occ_ras + SurveyQ_ras 
  paste0("Values of NO + PS: ", unique(values(ignorance_ras))) # [1] NA  1  0  3
  Surv_igno_ras <- ignorance_ras + sampbias_rec
  paste0("Values of ignorance layer: ", unique(values(Surv_igno_ras))) # [1] NA  1  0  3  5  6  8
  
  # Saving raster layers in GlobalEnv.
  assign(paste(coding_alt, "layerNO", "val", rec_NO, sep = "_"), occ_ras, envir = .GlobalEnv);
  assign(paste(coding_alt, "layerPS", "val", rec_PS, sep = "_"), SurveyQ_ras, envir = .GlobalEnv);
  assign(paste(coding_alt, "layerSB", "val", rec_SB, sep = "_"), sampbias_rec, envir = .GlobalEnv);
  assign(paste(coding_alt, "FinalRaster", sep = "_"), Surv_igno_ras, envir = .GlobalEnv)
  
  # Codification scheme
  Cod_scheme_df <- data.frame(
    row.names = 1:7,
    Cell_type = c("NO",  "PS", "NO + PS (*)", "SB", "NO + SB", "PS + SB", "NO + PS + SB (*)"),
    comb_value = c(rec_NO, rec_PS, rec_NO + rec_PS,  rec_SB,
                   rec_NO + rec_SB, rec_PS + rec_SB, rec_NO + rec_PS + rec_SB)
  )
  
  # Map
  par(mfrow = c(2, 2), mar = c(4, 4, 4, 4))
  plot(occ_ras)
  title(main = "No Occurrence (NO)", 
        sub = paste0("Reclassified value ", rec_NO,  "\n 0: land, NA: sea"),
        font.sub = 3, col.sub = "blue", cex.sub = 1.5)
  plot(SurveyQ_ras)
  title(main = "SurveyQ - Poor (PS)", 
        sub = paste0("Reclassified value ", rec_PS, "\n 0: everything else"),
        font.sub = 3, col.sub = "blue", cex.sub = 1.5)
  plot(sampbias_rec)
  title(main = "sampbias - lowest rates (SB)", 
        sub = paste0("Reclassified value ", rec_SB, "\n 0: land, NA: sea"),
        font.sub = 3, col.sub = "blue", cex.sub = 1.5)
  plot(Surv_igno_ras)
  title(main = "Final raster (Ignorance)", 
        sub = paste0("Cell types: ", 
                     paste(as.character(unique(values(Surv_igno_ras))), 
                           collapse = " "), "\n 0: land, NA: sea"),
        font.sub = 3, col.sub = "blue", cex.sub = 1.5)
  
  return(Cod_scheme_df)
  
}

# Original coding = NO:1 PS:3 SB:5 
checking_codification(rec_NO = 1, rec_PS = 3, rec_SB = 5, 
                      coding_alt = "Original")

# Alternate coding: NO:2 PS:4 SB:6
checking_codification(rec_NO = 2, rec_PS = 4, rec_SB = 6, 
                      coding_alt = "A246")

# Alternate coding: NO:1 PS:2 SB:3 
checking_codification(rec_NO = 1, rec_PS = 2, rec_SB = 3,
                      coding_alt = "A123")

# 4.3. Test two: number of cells per cell type ####

# If the number of cells (i.e. length) for each cell type is the same 
# irrespective of the codification scheme, then the numeric value of the 
# codification scheme does not affect the number of categories obtained. 
# Therefore, although the codification is arbitrary, the categories are 
# conserved and the aim of synthesizing all the analysis is fulfilled. 

# Below, we showed that the number of cells per cell type does not change among
# the alternative codification schemes analyzed. For each cell type the number 
# of cells is compared across codification schemes. Each line must result in a
# "TRUE" value. The unique comparison that will change to "FALSE" will be the 
# cell type "NO + PS (*)". This is so because the comparison will be confounded
# with the number of cells under the cell type "PS" (See Table 1 above). 

# The number of cells for "NO + PS(*) in the codification schemes A123 and A246 
# must be the same, and equal to the number of cells of the cell type "PS" that 
# will be 49. 

# 1. NO
length(Original_FinalRaster[Original_FinalRaster == 1]) == 
  length(A246_FinalRaster[A246_FinalRaster == 2])

length(Original_FinalRaster[Original_FinalRaster == 1]) == 
  length(A123_FinalRaster[A123_FinalRaster == 1])

length(A123_FinalRaster[A123_FinalRaster == 1]) == 
  length(A246_FinalRaster[A246_FinalRaster == 2])

# 2. PS
length(Original_FinalRaster[Original_FinalRaster == 3]) == 
  length(A246_FinalRaster[A246_FinalRaster == 4])

length(Original_FinalRaster[Original_FinalRaster == 3]) == 
  length(A123_FinalRaster[A123_FinalRaster == 2])

length(A123_FinalRaster[A123_FinalRaster == 2]) == 
  length(A246_FinalRaster[A246_FinalRaster == 4])

# 3. NO + PS(*)
length(Original_FinalRaster[Original_FinalRaster == 4]) == 
  length(A246_FinalRaster[A246_FinalRaster == 6]) # FALSE

length(Original_FinalRaster[Original_FinalRaster == 4]) == 
  length(A123_FinalRaster[A123_FinalRaster == 3]) # FALSE

length(A123_FinalRaster[A123_FinalRaster == 3]) == 
  length(A246_FinalRaster[A246_FinalRaster == 6]) # This must be TRUE.
length(A123_FinalRaster[A123_FinalRaster == 3]) # 49

# 4. SB
length(Original_FinalRaster[Original_FinalRaster == 5]) == 
  length(A246_FinalRaster[A246_FinalRaster == 6])

length(Original_FinalRaster[Original_FinalRaster == 5]) == 
  length(A123_FinalRaster[A123_FinalRaster == 3])

length(A123_FinalRaster[A123_FinalRaster == 3]) == 
  length(A246_FinalRaster[A246_FinalRaster == 6])

# 5. NO + SB
length(Original_FinalRaster[Original_FinalRaster == 6]) == 
  length(A246_FinalRaster[A246_FinalRaster == 8])

length(Original_FinalRaster[Original_FinalRaster == 6]) == 
  length(A123_FinalRaster[A123_FinalRaster == 4])

length(A123_FinalRaster[A123_FinalRaster == 4]) == 
  length(A246_FinalRaster[A246_FinalRaster == 8])

# 6. PS + SB
length(Original_FinalRaster[Original_FinalRaster == 8]) == 
  length(A246_FinalRaster[A246_FinalRaster == 10])

length(Original_FinalRaster[Original_FinalRaster == 8]) == 
  length(A123_FinalRaster[A123_FinalRaster == 5])

length(A123_FinalRaster[A123_FinalRaster == 5]) == 
  length(A246_FinalRaster[A246_FinalRaster == 10])


# 5. Play with different codification schemes ####

# Define the values as you wish:

x <- 1
y <- 2
z <- 5

Celltypes_df <- checking_codification(rec_NO = x, rec_PS = y, rec_SB = z,
                      coding_alt = "arbitrary")

Celltypes_df

# NO
length(Original_FinalRaster[Original_FinalRaster == 1]) == 
  length(arbitrary_FinalRaster[arbitrary_FinalRaster == Celltypes_df[1,2]])

# 2. PS
length(Original_FinalRaster[Original_FinalRaster == 3]) == 
  length(arbitrary_FinalRaster[arbitrary_FinalRaster == Celltypes_df[2,2]])

# 3. NO + PS(*)
length(Original_FinalRaster[Original_FinalRaster == 4]) == 
  length(arbitrary_FinalRaster[arbitrary_FinalRaster == Celltypes_df[3,2]])

# 4. SB
length(Original_FinalRaster[Original_FinalRaster == 5]) == 
  length(arbitrary_FinalRaster[arbitrary_FinalRaster == Celltypes_df[4,2]])

# 5. NO + SB
length(Original_FinalRaster[Original_FinalRaster == 6]) == 
  length(arbitrary_FinalRaster[arbitrary_FinalRaster == Celltypes_df[5,2]])

# 6. PS + SB
length(Original_FinalRaster[Original_FinalRaster == 8]) == 
  length(arbitrary_FinalRaster[arbitrary_FinalRaster == Celltypes_df[6,2]])

