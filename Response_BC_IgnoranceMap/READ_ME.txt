############################

Dear Reviewer #2,

In this R project you will find the data and the script to reproduce the raster layers reclassification used to produce the Ignorance Map.

To see the R script and the results please open the file "ResponseR2_BC.html" in the main directory: "Response_BC_IgnoranceMap/". This file will be opened by your internet browser where you will have the opportunity to navigate along the script and see the result of its execution. 

If you want to reproduce the R Script, please open the file "./Response_BC_IgnoranceMap.Rproj in the main the directory "Response_BC_IgnoranceMap/". This file will open RStudio where you can follow step by step the R script used to produce the raster layers and map. The R Script is saved in the directory "./scr/" and it can be openned through the option "Open file" in the the main RStudio's toolbar.

Thank you for your time and support,

Best regards, 

Juan Pablo

##########################
 
